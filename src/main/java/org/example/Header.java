package org.example;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {

    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement greetingElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");


    public void clickOnTheLoginButton() {

        System.out.println("Click on the Login button");
        loginButton.click();
    }

    public String getGreetingsMessage() {

        return greetingElement.text();
    }


    public void clickOnTHeWishlistIcon() {

        System.out.println("Clicking on the Wishlist button");
        wishlistButton.click();

    }


    public void clickOnTheShoppingBagIcon() {
        System.out.println("Clicking on the Shopping Bag icon");
        homePageButton.click();
    }

    public void clickOnTHeCartIcon() {

        System.out.println("Click on the Cart icon");
        cartIcon.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingCartBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }

}
