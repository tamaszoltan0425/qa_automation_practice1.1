package org.example;


import com.codeborne.selenide.SelenideElement;

public class Main {

    public static void main(String[] args) {

        System.out.println(" == Demo Shop == ");
        System.out.println("1. User must be able to login with valid credentials.");

        Page page = new Page();
        page.openHomePage();

        Header header = new Header();
        header.clickOnTheLoginButton();

        ModalDialog modalDialog = new ModalDialog();
        modalDialog.typeInUsername("dino");
        modalDialog.typeInPassword("choochoo");
        modalDialog.clickOnTheLoginButton();

        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        //validate
        System.out.println("Hi dino is displayed in header " + isLogged);
        System.out.println("Greeting message is: " + greetingsMessage);

        System.out.println("--------------------------------------");
        System.out.println("2. User can add product to cart from product cards. ");
        page.openHomePage();
        ProductCards cards = new ProductCards();
//        Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
//        System.out.println("Product is: " + awesomeGraniteChips.getTitle());
//        awesomeGraniteChips.clickOnProductCartIcon();

        System.out.println("-------------------------------------");
        System.out.println("3. User can add products to Wishlist from product cards. ");
        page.openHomePage();
//        SelenideElement awesomeGraniteChipsWishlist = cards.getProductById("Awesome Granite Chips");
//        System.out.println("Product is: " + awesomeGraniteChipsWishlist.getTitle());
//        awesomeGraniteChipsWishlist.clickOnProductWishlistIcon();
//
//        System.out.println("-------------------------------------");
//        System.out.println("4. User can access product page from product cards. ");
//        page.openHomePage();
//        Product product1 = cards.getProductByName("Awesome Granite Chips");
//        System.out.println("Selected product is: " + product1.getTitle());
//        System.out.println("Opening: " + product1.getProductUrl());

        System.out.println("------------------------------------");
        System.out.println("5. User can navigate to homepage from wishlist page ");
        page.openHomePage();
        header.clickOnTHeWishlistIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on wishlist page. Page title is:  " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on homepage. Page title is: " + pageTitle);


        System.out.println("------------------------------------");
        System.out.println("6. User can navigate to homepage from cart page ");
        page.openHomePage();
        header.clickOnTHeCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on cart page. Page title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on homepage. Page title is: " + pageTitle);



    }
}
// Testing Login functionality:

// 1. Open home page
// 2. Click on Login button
// 3. Click on Username field
// 4. Type in Dino
// 5. Click on password field
// 6. Type in choochoo
// 7. Click on Login button
// Expected: Hi dino is displayed in header

// User can add product to cart from product card
// 1. Open home page
// 2. Click on the product cart icon
// Expected result: minicart icon in header shows 1

//User can add product to wishlist from product card
// 1. Open home page
// 2. Click on the "Awesome Granite Chips" wishlist icon
// Expected results: Wishlist icon in header shows 1 item in wishlist

//User can access product page from product card
// 1. Open home page
// 2. Click on the "Awesome Granite Chips" link on product card
// Expected result: "Awesome Granite Chips" product page is loaded

//User can navigate to homepage from wishlist page
//1. Open home page
//2. Click on the "Favorites" icon
//3. Click on "Shopping Bag" icon
//Expected: Homepage is loaded