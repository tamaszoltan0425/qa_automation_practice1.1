package org.example;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductCards {

    private final ElementsCollection cards = $$(".card");
    private final SelenideElement sortButton = $(".sort-products-select");
    private final SelenideElement sortAZ = $("option[value=az]");
    private final SelenideElement sortZA = $("option[value=za]");
    private final SelenideElement sortLoHI = $("option[value=lohi]");
    private final SelenideElement sortHiLo = $("option[value=hilo]");


    public ProductCards() {

    }

    public Product getProductById(String productId) {
        for (SelenideElement product : cards) {
            if (product.$(".card-link").getAttribute("href").endsWith("/" + productId)) {
                return new Product(product);
            }
        }
        return null;
    }
    public void clickSortButton() {
        this.sortButton.click();
    }

    public void clickOnTheAZSortButton() {
        this.sortAZ.click();
    }
    public Product getFirstProductInList() {
        SelenideElement first = cards.first();
        return new Product(first);

    }
    public Product getLastProductInList() {
        SelenideElement last = cards.last();
        return new Product(last);
    }

    public void clickOnTheZASortButton() {
        this.sortZA.click();
    }

    public void clickOnLoHi() {
        this.sortLoHI.click();
    }
    public void clickOnHiLo() {
        this.sortHiLo.click();
    }

}
