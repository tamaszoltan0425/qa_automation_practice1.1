package org.example;

import static org.testng.Assert.*;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterMethod;
//import static org.testng.Assert.assertEquals;
//import static org.testng.Assert.assertFalse;

public class CartManagementTest {

    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    public void when_user_navigates_to_cart_page_empty_cart_message_is_shown() {
        header.clickOnTHeCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }
    @Test
    public void when_adding_one_product_to_cart_empty_cart_message_is__not_shown() {
        Product product = new Product("9");
        product.clickOnProductCartIcon();
        header.clickOnTHeCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }

    @Test
    public void user_can_increment_the_amount_of_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnProductCartIcon();
        header.clickOnTHeCartIcon();
        CartItem cartItem = new CartItem("9");
        cartItem.increaseAmount();
        assertEquals(cartItem.getItemAmount(), "2");
    }

    @Test
    public void user_can_reduce_the_amount_of_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnProductCartIcon();
        product.clickOnProductCartIcon();
        header.clickOnTHeCartIcon();
        CartItem cartItem = new CartItem("9");
        cartItem.reduceAmount();
        assertEquals(cartItem.getItemAmount(), "1");
    }
}
