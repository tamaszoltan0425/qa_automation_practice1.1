package org.example;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MainTest {

    Page page = new Page();
    Header header = new Header();
    ModalDialog modalDialog = new ModalDialog();



    @BeforeClass
    public void setup() {
        page.openHomePage();
    }
    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToReset();
    }
    @Test
    public void user_turtle_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modalDialog.typeInUsername("turtle");
        modalDialog.typeInPassword("choochoo");
        modalDialog.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "Logged in with turtle, expected greetings message to be 'Hi turtle!' ");
    }

    @Test
    public void user_dino_can_login_with_valid_credentials() {
        header.clickOnTheLoginButton();
        modalDialog.typeInUsername("dino");
        modalDialog.typeInPassword("choochoo");
        modalDialog.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi dino!", "Logged in with dino, expected greetings message to be 'Hi dino!' ");
    }

    @Test

    public void user_can_navigate_to_Wishlist_page() {
        header.clickOnTHeWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on Wishlist page");
    }

    @Test
    public void user_can_navigate_to_cart_page() {
        header.clickOnTHeCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page");

    }

    @Test
    public void user_can_navigate_to_product_page_from_cart_page() {
        header.clickOnTHeCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page");
    }

    @Test
    public void user_can_navigate_to_product_page_from_wishlist_page() {
        header.clickOnTHeWishlistIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page");
    }

    @Test
    public void user_can_add_products_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnProductCartIcon();
        assertTrue(header.isShoppingCartBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding 1 product to cart, badge shows 1.");

    }

    @Test
    public void user_can_add_two_products_to_cart_from_product_cards() {
        Product product = new Product("9");
        product.clickOnProductCartIcon();
        product.clickOnProductCartIcon();
        assertTrue(header.isShoppingCartBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding 2 products to cart, badge shows 2.");

    }


}